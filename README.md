# 应用程序配置文件定义规范

## 1. index.yml

index.yml 是本仓库中所有程序配置文件的汇总，每一个具体程序的 yml 配置文件必须在 index.yml 中注册完成才能被索引到。

index.yml 中配置信息格式如下所示：

```
Key:   # 每个应用的唯一标识，且必须与 Category对应的文件夹中的文件名称匹配，如果有desktop文件，则该名字是desktop对应的名字
  Arch: win32 # exe程序对应的架构，win32或win64
  Name: 显示名称
  Description: 程序描述性信息
  Grade: Gold # 适配等级，Gold，Silver，Bronze
  Category: Software # 程序类别，目前支持Game、Software
  Icon: # 图标网络地址，为空时，wine助手使用 wine助手自己图标代替显示
  LinuxArch:    # 该程序可以适配的平台，根据具体测试情况定
    - x86_64
    - aarch64

# 下面是具体应用的示例
YNotes桌面便签:
  Arch: win32
  Name: YNotes桌面便签
  Description: YNotes是Ysofts出品的一款桌面便签小软件,它使用方便,界面简洁,功能较全,非常实用。
  Grade: Gold
  Category: Software
  Icon: default.png
  LinuxArch:
    - x86_64
    - aarch64

StarUML:
  Arch: win32
  Name: StarUML
  Description: StarUML(简称SU)，是一种创建UML类图，生成类图和其他类型的统一建模语言(UML)图表的工具。
  Grade: Gold
  Category: Software
  Icon: StarUML.png
  LinuxArch:
    - x86_64
    - aarch64
  Publish: true #是否发布，如在测试中的应用可以将此致设置为false，使其不在终端展示。
```

## 2. 应用实际对应的配置

应用详细的配置文件以 index.yml 中的 Key 为文件名，存储在仓库中 Category 对应的文件夹下。具体示例如下:

```
$ cat win-program/Software/StarUML.yml
    |  仓库名    |Category|     Key    |    <---对应index.yml

Name: StarUML
Description: StarUML
Grade: Silver
Arch: win32
LinuxArch:
- x86_64
- aarch64
Icon: # 选填 图标网络地址，防止wine助手解析启动文件获取图标失败或不正常。不为空时，wine助手下载该图标制作Desktop文件，否则wine助手自己解析启动文件获取图标。
Dependencies:
- commondlls
Executable:             # 程序启动对应的参数
  run_in_window: true #应用是否运行在窗口模式，取值范围true或者800x600
  name: StarUML.exe    # 重要：应用安装后的启动程序，用来监听应用是否安装
  path: Program Files (x86)/xxx/xx.exe  # 应用运行路径, dos 格式，可以为空
  wineenv: #wine运行时env，可以为空，以kv的格式定义，取值范围为wine的环境变量
  arguments: #应用运行是参数，可以为空
Steps:                  # action支持 install
- action: install       # 实际的安装动作
  file_type: 7z #文件类型，目前支持msi,exe,7z,rar,zip
  #下载的安装文件为压缩包时，file_name要填写解压后要执行的exe文件路径，填写相对于解压目录的路径。
  file_name: StarUML_Setup_3.2.2/StarUML Setup 3.2.2.exe
  url: https://dl.softmgr.qq.com/original/Picture/StarUML_Setup_3.2.2.7z
  file_checksum: 'false'
  arguments: null
  protable: true        # 若不是绿色软件，则false
```

## 3. 其他注意事项

### （1）常用解压命令

常见类型及解压命令：
```
zip：unzip -O GBK -q $filePath -d $destDir
rar：unrar e -y $filePath $destDir
7z：7z x -y $filePath -o$destDir
cab：cabextract -d $destDir -q $filePath
gz：tar -xf $filePath -C $destDir
xz：tar -xf $filePath -C $destDir
bz2：bzip2 -d $filePath -c |tar -xf - -C $destDir

其中，$filePath和$destDir分别表示要解压的文件路径和临时解压目录，临时解压路径在填写配置时要忽略。
```